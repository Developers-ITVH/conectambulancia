package com.bringsolutions.conectambulancia.Adaptadores;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bringsolutions.conectambulancia.objetos.Mensajes;
import com.bringsolutions.conectambulancia.R;

import java.util.List;


public class AdaptadorMensajes extends  RecyclerView.Adapter<AdaptadorMensajes.ViewHolder>
{

    private List<Mensajes> mensajesList;
    private Context context;

    public AdaptadorMensajes(List<Mensajes> mensajesList, Context context)
    {
        this.mensajesList = mensajesList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tarjetachat,viewGroup,false);
        return new ViewHolder(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i)
    {

        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(viewHolder.caja.getLayoutParams());

        if (mensajesList.get(i).getUsuario().equals("Bot"))
        {


                  /* lp.setMargins(0, 20, 5, 0);
                   viewHolder.mensaje.setText((mensajesList.get(i).getMensaje()));
                   viewHolder.caja.setLayoutParams(lp);
                   viewHolder.mensaje.setBackgroundResource(R.drawable.botmensaje);
                   viewHolder.mensaje.setTextColor(Color.WHITE);
              */

                   lp.setMargins(0, 20, 5, 0);
                   viewHolder.mensaje.setText((mensajesList.get(i).getMensaje()));
                   viewHolder.caja.setLayoutParams(lp);
                   viewHolder.mensaje.setBackgroundResource(R.drawable.botmensaje);
                   viewHolder.mensaje.setTextColor(Color.WHITE);
                    if (!mensajesList.get(i).getLink().equals("sn"))
                    {
                        viewHolder.webView.loadUrl(mensajesList.get(i).getLink());

                    }





        }else
            {

                viewHolder.mensaje.setText((mensajesList.get(i).getMensaje()));
                viewHolder.nombre.setVisibility(View.GONE);

            }


    }

    @Override
    public int getItemCount()
    {
        return mensajesList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {

        private TextView mensaje,nombre;
        private RelativeLayout caja;
        private WebView webView;


        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);

            mensaje = itemView.findViewById(R.id.texto);
            nombre  = itemView.findViewById(R.id.name);
            caja    = itemView.findViewById(R.id.caja);
            webView = itemView.findViewById(R.id.webviewbot);
            //Habilitamos JavaScript
            webView.getSettings().setJavaScriptEnabled(true);

            //Habilitamos los botones de Zoom
            webView.getSettings().setBuiltInZoomControls(true);

            webView.setWebViewClient(new WebViewClient() {

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;
                }
            });



        }
    }
}
