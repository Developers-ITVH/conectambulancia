package com.bringsolutions.conectambulancia.objetos;

public class Doctor {
	String tipo, nombre, horario, descripcion, telefono;
	
	public Doctor(){}
	
	public Doctor(String tipo, String nombre, String horario, String descripcion, String telefono) {
		this.tipo = tipo;
		this.nombre = nombre;
		this.horario = horario;
		this.descripcion = descripcion;
		this.telefono = telefono;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getHorario() {
		return horario;
	}
	
	public void setHorario(String horario) {
		this.horario = horario;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public String getTelefono() {
		return telefono;
	}
	
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
}
