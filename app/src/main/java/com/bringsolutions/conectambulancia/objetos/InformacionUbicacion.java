package com.bringsolutions.conectambulancia.objetos;
public class InformacionUbicacion {
	Double Latitud;
	Double Longitud;
	
	
	public InformacionUbicacion(){}
	
	public InformacionUbicacion(Double latitud, Double longitud) {
		Latitud = latitud;
		Longitud = longitud;
	}
	
	public Double getLatitud() {
		return Latitud;
	}
	
	public void setLatitud(Double latitud) {
		Latitud = latitud;
	}
	
	public Double getLongitud() {
		return Longitud;
	}
	
	public void setLongitud(Double longitud) {
		Longitud = longitud;
	}
}
