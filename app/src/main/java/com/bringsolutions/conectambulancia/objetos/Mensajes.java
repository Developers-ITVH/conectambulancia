package com.bringsolutions.conectambulancia.objetos;

public class Mensajes
{
    private String usuario ;
    private String mensaje;
    private String link="sd";

    public Mensajes(String usuario, String mensaje)
    {
        this.usuario = usuario;
        this.mensaje = mensaje;
    }

    public Mensajes(String usuario, String mensaje, String link)
    {
        this.usuario = usuario;
        this.mensaje = mensaje;
        this.link = link;
    }

    public String getUsuario()
    {
        return usuario;
    }

    public void setUsuario(String usuario)
    {
        this.usuario = usuario;
    }

    public String getMensaje()
    {
        return mensaje;
    }

    public void setMensaje(String mensaje)
    {
        this.mensaje = mensaje;
    }

    public String getLink()
    {
        return link;
    }

    public void setLink(String link)
    {
        this.link = link;
    }
}
