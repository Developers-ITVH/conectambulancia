package com.bringsolutions.conectambulancia.objetos;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.bringsolutions.conectambulancia.R;
import java.util.List;

public class AdaptadorDoctores extends RecyclerView.Adapter<AdaptadorDoctores.ViewHolder> {
	
	//clase viewholder para enlazar componesntes con la vista de los mensajes
	public static class ViewHolder extends RecyclerView.ViewHolder{
		
		private TextView nombre, horario,descripcion;
		private CardView tarjetaDoctor;
		
		private de.hdodenhof.circleimageview.CircleImageView fotovendedor;
		private Button what,llamada;
		
		public ViewHolder(View itemView) {
			super(itemView);
			//enlanzando elementos
			nombre = itemView.findViewById(R.id.tvNombreDoctorTarjeta);
			horario = itemView.findViewById(R.id.tvHorarioDoctorTarjeta);
			descripcion = itemView.findViewById(R.id.tvDescripcionDoctorTarjeta);
			tarjetaDoctor = itemView.findViewById(R.id.tarjetita_doctor);
			what = itemView.findViewById(R.id.btnWhatsTarjetaDoctor);
			llamada = itemView.findViewById(R.id.btnLlamarTarjetaDoctor);
			
		}
	}
	
	public List<Doctor> doctoresLista;
	public Context context;
	
	public AdaptadorDoctores(List<Doctor> doctorsList, Context context) {
		this.doctoresLista = doctorsList;
		this.context = context;
	}
	
	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
		View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tarjeta_vol,viewGroup,false);
		
		return new ViewHolder(view);
	} @Override
	public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final  int i) {
		viewHolder.nombre.setText(doctoresLista.get(i).getNombre());
		viewHolder.horario.setText(doctoresLista.get(i).getHorario());
		viewHolder.descripcion.setText(doctoresLista.get(i).getDescripcion());
		
		viewHolder.llamada.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				context.startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+doctoresLista.get(i).getTelefono())));
				
				
			}
		});
		
		viewHolder.what.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Uri uri = Uri.parse("https://api.whatsapp.com/send?phone=52"+doctoresLista.get(i).getTelefono()+"&text=Hola "+doctoresLista.get(i).getNombre()+"! Estoy interesado el los servicios médicos que ofrece");
				context.startActivity( new Intent(Intent.ACTION_VIEW, uri));
				Toast.makeText(context, "Se abrirá en WhatsApp", Toast.LENGTH_SHORT).show();
			
			}
		});
	}
	
	
	
	
	@Override
	public int getItemCount() {
		return doctoresLista.size();
	}
	
	
}