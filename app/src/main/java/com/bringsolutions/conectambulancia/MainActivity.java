package com.bringsolutions.conectambulancia;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.MenuItem;
import android.view.View;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.Toast;
import com.bringsolutions.conectambulancia.ambulancias.LoginAmbulancia;

public class MainActivity extends AppCompatActivity {
	
	private AppBarConfiguration mAppBarConfiguration;
	
	final int PERMISO_ACCESS_COARSE_LOCATION =1;
	final int PERMISO_ACCESS_FINE_LOCATION =2;
	final int PERMISO_INTERNET =3;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		
		FloatingActionButton fab = findViewById(R.id.fab);
		
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Snackbar.make(view, "Sistema Desarrollado por BringSolutions", Snackbar.LENGTH_LONG)
						.setAction("Action", null).show();
			}
		});
		DrawerLayout drawer = findViewById(R.id.drawer_layout);
		NavigationView navigationView = findViewById(R.id.nav_view);
		
		
		mAppBarConfiguration = new AppBarConfiguration.Builder(
				R.id.nav_ambulancia, R.id.nav_quehacer, R.id.nav_preguntame,
				R.id.nav_ayudavol, R.id.nav_contacto, R.id.nav_instituciones)
				.setDrawerLayout(drawer)
				.build();
		
		
		NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
		
		NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
		NavigationUI.setupWithNavController(navigationView, navController);
	}
	
	private void verificarPermisos() {
		if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION )
				!= PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION )
				!= PackageManager.PERMISSION_GRANTED) {
			
			new AlertDialog.Builder(this)
					.setTitle("Autorización")
					.setMessage("Es necesario saber su ubicación para utilizar los servicios de localización")
					.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							
							if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
								requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISO_ACCESS_COARSE_LOCATION);
								requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISO_ACCESS_FINE_LOCATION);
								requestPermissions(new String[]{Manifest.permission.INTERNET}, PERMISO_INTERNET);
								
							}
							
						}
					})
					.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							//Mensaje acción cancelada
							Toast.makeText(getApplicationContext(),"Sin su ubicación algunos servicios no funcionarán", Toast.LENGTH_LONG).show();					}
					})
					.show();
		}
		
		
		
		/*
		
		int verificarPermisoCoarse_Location = ContextCompat
				.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
		
		int verificarFine_Location = ContextCompat
				.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
		
		int verificarInterner = ContextCompat
				.checkSelfPermission(this, Manifest.permission.INTERNET);
		
		//si la API 23 a mas
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			//Habilitar permisos para la version de API 23 a mas
			
			//Verificamos si el permiso no existe
			if (verificarPermisoCoarse_Location != PackageManager.PERMISSION_GRANTED) {
				//verifico si el usuario a rechazado el permiso anteriormente
				if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)) {
					//Si a rechazado el permiso anteriormente muestro un mensaje
					Toast.makeText(this, "Es necesaria la ubicación para utilizar los servicios", Toast.LENGTH_LONG).show();
				} else {
					//De lo contrario carga la ventana para autorizar el permiso
					requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISO_ACCESS_COARSE_LOCATION);
				}
				
			}
			
			//Verificamos si el permiso no existe
			if (verificarFine_Location != PackageManager.PERMISSION_GRANTED) {
				//verifico si el usuario a rechazado el permiso anteriormente
				if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
					//Si a rechazado el permiso anteriormente muestro un mensaje
					Toast.makeText(this, "Es necesaria la ubicación para utilizar los servicios", Toast.LENGTH_LONG).show();
				} else {
					//De lo contrario carga la ventana para autorizar el permiso
					requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISO_ACCESS_FINE_LOCATION);
				}
				
			}
			
			//Verificamos si el permiso no existe
			if (verificarFine_Location != PackageManager.PERMISSION_GRANTED) {
				//verifico si el usuario a rechazado el permiso anteriormente
				if (shouldShowRequestPermissionRationale(Manifest.permission.INTERNET)) {
					//Si a rechazado el permiso anteriormente muestro un mensaje
					Toast.makeText(this, "Es necesario el internet para utilizar los servicios", Toast.LENGTH_LONG).show();
				} else {
					//De lo contrario carga la ventana para autorizar el permiso
					requestPermissions(new String[]{Manifest.permission.INTERNET}, PERMISO_INTERNET);
				}
				
			}
			
			
		}
		*/
		
	
	
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		
		return true;
	}
	
	@Override
	public boolean onSupportNavigateUp() {
		NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
		
		return NavigationUI.navigateUp(navController, mAppBarConfiguration)	|| super.onSupportNavigateUp();
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.ambulancia_item:
				startActivity(new Intent(MainActivity.this, LoginAmbulancia.class));

				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
}
