package com.bringsolutions.conectambulancia.ui.preguntame;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class PreguntameViewModel extends ViewModel {
	
	private MutableLiveData<String> mText;
	
	public PreguntameViewModel() {
		mText = new MutableLiveData<>();
		mText.setValue("This is slideshow fragment");
	}
	
	public LiveData<String> getText() {
		return mText;
	}
}