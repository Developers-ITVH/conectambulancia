package com.bringsolutions.conectambulancia.ui.quehacer;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class QueHacerViewModel extends ViewModel {
	
	private MutableLiveData<String> mText;
	
	public QueHacerViewModel() {
		mText = new MutableLiveData<>();
		mText.setValue("Qué hacer en caso de");
	}
	
	public LiveData<String> getText() {
		return mText;
	}
}