package com.bringsolutions.conectambulancia.ui.contacto;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.support.annotation.Nullable;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;

import com.bringsolutions.conectambulancia.R;

public class ContactoFragment extends Fragment {
	
	private ContactoViewModel contactoViewModel;
	
	public View onCreateView(@NonNull LayoutInflater inflater,
							 ViewGroup container, Bundle savedInstanceState) {
		contactoViewModel =
				ViewModelProviders.of(this).get(ContactoViewModel.class);
		View root = inflater.inflate(R.layout.fragment_contacto, container, false);
		final TextView textView = root.findViewById(R.id.text_share);
		contactoViewModel.getText().observe(this, new Observer<String>() {
			@Override
			public void onChanged(@Nullable String s) {
				textView.setText(s);
			}
		});
		return root;
	}
}