package com.bringsolutions.conectambulancia.ui.ambulancia;
//bien

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.widget.Button;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bringsolutions.conectambulancia.R;
import com.bringsolutions.conectambulancia.objetos.Constantes;
import com.bringsolutions.conectambulancia.objetos.InformacionUbicacion;
import com.bringsolutions.conectambulancia.objetos.Utilidades;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;


public class AmbulanciaFragment extends Fragment implements LocationListener {
	FloatingActionButton botonRutaFlotanto;
	PolylineOptions lineOptions = null;
	
	Double latiAmbu , longiAmbu;
	Double mLati, mLongi;
	
	View view;
	Button btnSolicitarAmbulancia;
	DatabaseReference referenciaDatabase;
	SupportMapFragment mapFragment;
	
	//ELEMENTOS PARA VOLLEY
	RequestQueue requestQueue;
	StringRequest stringRequest;
	
	JsonObjectRequest jsonObjectRequest;
	RequestQueue request;
	
	GoogleMap mapilla;
	
	
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fragment_ambulancia, container, false);
		inicializarElementos();
		
		btnSolicitarAmbulancia.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (btnSolicitarAmbulancia.getText().toString().equals("Solicitar Ambulancia")) {
					btnSolicitarAmbulancia.setText("Verificar Ruta");
					enviarMiUbicacionActualFirebase();
					
				} else if (btnSolicitarAmbulancia.getText().toString().equals("Verificar Ruta")) {
					
					final Handler handler = new Handler();
					
					Timer timer = new Timer();
					
					TimerTask task = new TimerTask() {
						@Override
						public void run() {
							handler.post(new Runnable() {
								@Override
								public void run() {
									try{
										Utilidades.routes.clear();
										webServiceObtenerRuta(String.valueOf(mLati), String.valueOf(mLongi), String.valueOf(latiAmbu), String.valueOf(longiAmbu));
									}catch (Exception e){
										Toast.makeText(getContext(), "Posible error: " +e.toString(), Toast.LENGTH_SHORT).show();
									}
									
								}
							});
						}
					};
					timer.schedule(task,0,5000);
					
					
				
				}
				
			}
		});
		
		
		return view;
	}
	
	private void enviarMiUbicacionActualFirebase() {
		verificarPermisosUbicacionTelefono();
		
	}
	
	
	private void verificarPermisosUbicacionTelefono() {
		if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
		} else {
			extraerUbicacion();
		}
		
	}
	
	private void extraerUbicacion() {
		LocationManager mlocManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
		final boolean gpsEnabled = mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		if (!gpsEnabled) {
			Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivity(settingsIntent);
		}
		if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
			return;
		}
		
		Location location = mlocManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		
		Map<String, Object> latitudLongitud = new HashMap<>();
		latitudLongitud.put("Latitud", location.getLatitude());
		latitudLongitud.put("Longitud", location.getLongitude());
		
		referenciaDatabase.child("Usuarios").child("Alejo").setValue(latitudLongitud);
		
		marcarUbicacionPropia();
		marcarUbicacionAmbulancia();

		//mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
		//mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,  this);
		
	}
	
	private void marcarUbicacionPropia(){
		
		mapFragment.getMapAsync(new OnMapReadyCallback() {
			@Override
			public void onMapReady(final GoogleMap googleMap) {
				
				referenciaDatabase.child("Usuarios").child("Alejo").addListenerForSingleValueEvent(new ValueEventListener() {
					@Override
					public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
						String dir="";
						InformacionUbicacion informacionUbicacion = dataSnapshot.getValue(InformacionUbicacion.class);
						Location loc = new Location("");
						loc.setLatitude(informacionUbicacion.getLatitud());
						loc.setLongitude(informacionUbicacion.getLongitud());
						
						mLati=loc.getLatitude();
						mLongi =loc.getLongitude();
						
						if (loc.getLatitude() != 0.0 && loc.getLongitude() != 0.0) {
							try {
								Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
								List<Address> list = geocoder.getFromLocation(
										loc.getLatitude(), loc.getLongitude(), 1);
								if (!list.isEmpty()) {
									Address DirCalle = list.get(0);
									
									dir = DirCalle.getAddressLine(0);
								}
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
						MarkerOptions markerOptions  = new MarkerOptions();
						markerOptions.title(dir);
						markerOptions.position(new LatLng(loc.getLatitude(), loc.getLongitude()));
						//realizarSolicitudAmbulancia(dir);
						googleMap.addMarker(markerOptions);
						googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(loc.getLatitude(), loc.getLongitude()), 10));
						
						
					}
					
					@Override
					public void onCancelled(@NonNull DatabaseError databaseError) {
					
					}
				});
				
			}
		});
		
		
		
		
	
	}
	
	private void marcarUbicacionAmbulancia() {
		
		mapFragment.getMapAsync(new OnMapReadyCallback() {
			@Override
			public void onMapReady(final GoogleMap mMap) {
				mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
				mapilla=mMap;
				
				referenciaDatabase.child("Ambulancias").child("A-01").addValueEventListener(new ValueEventListener() {
					@Override
					public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
						mMap.clear(); //borro marcadores viejos
						marcarUbicacionPropia();
						
						InformacionUbicacion informacionUbicacion = dataSnapshot.getValue(InformacionUbicacion.class);
						Location loc = new Location("");
						loc.setLatitude(informacionUbicacion.getLatitud());
						loc.setLongitude(informacionUbicacion.getLongitud());
						
						MarkerOptions markerOptions  = new MarkerOptions();
						markerOptions.icon(bitmapDescriptorFromVector(getActivity(), R.drawable.marker_ambulancia));
						
						markerOptions.title("A-01");
						markerOptions.position(new LatLng(loc.getLatitude(), loc.getLongitude()));
						mMap.addMarker(markerOptions);
						
						latiAmbu=loc.getLatitude();
						longiAmbu=loc.getLongitude();
						
						
						
					}
					
					@Override
					public void onCancelled(@NonNull DatabaseError databaseError) {
					
					}
				});
				
				
				
			}
		});
		
		
		
	}
	
	private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
		Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
		vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
		Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		vectorDrawable.draw(canvas);
		return BitmapDescriptorFactory.fromBitmap(bitmap);
	}
	
	private void webServiceObtenerRuta(String latitudInicial, String longitudInicial, String latitudFinal, String longitudFinal) {
		String url = "https://maps.googleapis.com/maps/api/directions/json?origin="+latitudInicial+","+longitudInicial+"&destination="+latitudFinal+","+longitudFinal+"&sensor=false&mode=driving&key=AIzaSyD4imXRZLBQAUCJq7YwbtIIWpdtCtMFhns";
		
		jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response) {
				//Este método PARSEA el JSONObject que retorna del API de Rutas de Google devolviendo
				//una lista del lista de HashMap Strings con el listado de Coordenadas de Lat y Long,
				//con la cual se podrá dibujar pollinas que describan la ruta entre 2 puntos.
				JSONArray jRoutes = null;
				JSONArray jLegs = null;
				JSONArray jSteps = null;
				
				try {
					
					jRoutes = response.getJSONArray("routes");
					
					/** Traversing all routes */
					for(int i=0;i<jRoutes.length();i++){
						jLegs = ( (JSONObject)jRoutes.get(i)).getJSONArray("legs");
						List<HashMap<String, String>> path = new ArrayList<HashMap<String, String>>();
						
						/** Traversing all legs */
						for(int j=0;j<jLegs.length();j++){
							jSteps = ( (JSONObject)jLegs.get(j)).getJSONArray("steps");
							
							/** Traversing all steps */
							for(int k=0;k<jSteps.length();k++){
								String polyline = "";
								polyline = (String)((JSONObject)((JSONObject)jSteps.get(k)).get("polyline")).get("points");
								List<LatLng> list = decodePoly(polyline);
								
								/** Traversing all points */
								for(int l=0;l<list.size();l++){
									HashMap<String, String> hm = new HashMap<String, String>();
									hm.put("lat", Double.toString(((LatLng)list.get(l)).latitude) );
									hm.put("lng", Double.toString(((LatLng)list.get(l)).longitude) );
									
									path.add(hm);
								}
							}
							Utilidades.routes.add(path);
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}catch (Exception e){
				}
				
				
				pintarRuta(mapilla);
				
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				Toast.makeText(getContext(), "No se puede conectar "+error.toString(), Toast.LENGTH_LONG).show();
				System.out.println();
				Log.d("ERROR: ", error.toString());
			}
		}
		);
		
		request.add(jsonObjectRequest);
	}
	
	public List<List<HashMap<String,String>>> parse(JSONObject jObject){
		//Este método PARSEA el JSONObject que retorna del API de Rutas de Google devolviendo
		//una lista del lista de HashMap Strings con el listado de Coordenadas de Lat y Long,
		//con la cual se podrá dibujar pollinas que describan la ruta entre 2 puntos.
		JSONArray jRoutes = null;
		JSONArray jLegs = null;
		JSONArray jSteps = null;
		
		try {
			
			jRoutes = jObject.getJSONArray("routes");
			
			/** Traversing all routes */
			for(int i=0;i<jRoutes.length();i++){
				jLegs = ( (JSONObject)jRoutes.get(i)).getJSONArray("legs");
				List<HashMap<String, String>> path = new ArrayList<HashMap<String, String>>();
				
				/** Traversing all legs */
				for(int j=0;j<jLegs.length();j++){
					jSteps = ( (JSONObject)jLegs.get(j)).getJSONArray("steps");
					
					/** Traversing all steps */
					for(int k=0;k<jSteps.length();k++){
						String polyline = "";
						polyline = (String)((JSONObject)((JSONObject)jSteps.get(k)).get("polyline")).get("points");
						List<LatLng> list = decodePoly(polyline);
						
						/** Traversing all points */
						for(int l=0;l<list.size();l++){
							HashMap<String, String> hm = new HashMap<String, String>();
							hm.put("lat", Double.toString(((LatLng)list.get(l)).latitude) );
							hm.put("lng", Double.toString(((LatLng)list.get(l)).longitude) );
							path.add(hm);
						}
					}
					Utilidades.routes.add(path);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}catch (Exception e){
		}
		return Utilidades.routes;
	}
	
	private List<LatLng> decodePoly(String encoded) {
		
		List<LatLng> poly = new ArrayList<LatLng>();
		int index = 0, len = encoded.length();
		int lat = 0, lng = 0;
		
		while (index < len) {
			int b, shift = 0, result = 0;
			do {
				b = encoded.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);
			int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lat += dlat;
			
			shift = 0;
			result = 0;
			do {
				b = encoded.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);
			int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lng += dlng;
			
			LatLng p = new LatLng((((double) lat / 1E5)),
					(((double) lng / 1E5)));
			poly.add(p);
		}
		
		return poly;
	}
	
	private void pintarRuta(GoogleMap mMap) {
		lineOptions = new PolylineOptions();
		
		/////////////
		LatLng center = null;
		ArrayList<LatLng> points = null;
		points = new ArrayList<LatLng>();
		
		// recorriendo todas las rutas
		for(int i=0;i<Utilidades.routes.size();i++){
			// Obteniendo el detalle de la ruta
			List<HashMap<String, String>> path = Utilidades.routes.get(i);
			
			// Obteniendo todos los puntos y/o coordenadas de la ruta
			for(int j=0;j<path.size();j++){
				HashMap<String,String> point = path.get(j);
				
				double lat = Double.parseDouble(point.get("lat"));
				double lng = Double.parseDouble(point.get("lng"));
				LatLng position = new LatLng(lat, lng);
				
				if (center == null) {
					//Obtengo la 1ra coordenada para centrar el mapa en la misma.
					center = new LatLng(lat, lng);
				}
				points.add(position);
			}
			
			// Agregamos todos los puntos en la ruta al objeto LineOptions
			lineOptions.addAll(points);
			//Definimos el grosor de las Polilíneas
			lineOptions.width(5);
			//Definimos el color de la Polilíneas
			lineOptions.color(Color.RED);
			
		}
		
		// Dibujamos las Polilineas en el Google Map para cada ruta
		mMap.addPolyline(lineOptions);
		/*
		LatLng origen = new LatLng(Utilidades.coordenadas.getLatitudInicial(), Utilidades.coordenadas.getLongitudInicial());
		mMap.addMarker(new MarkerOptions().position(origen).title("Lat: "+Utilidades.coordenadas.getLatitudInicial()+" - Long: "+Utilidades.coordenadas.getLongitudInicial()));
		
		LatLng destino = new LatLng(Utilidades.coordenadas.getLatitudFinal(), Utilidades.coordenadas.getLongitudFinal());
		mMap.addMarker(new MarkerOptions().position(destino).title("Lat: "+Utilidades.coordenadas.getLatitudFinal()+" - Long: "+Utilidades.coordenadas.getLongitudFinal()));
		*/
		//mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(destino, 15));
		
	}
	
	
	private void inicializarElementos() {
		
		btnSolicitarAmbulancia = view.findViewById(R.id.btnSolicitarAmbulancia);
		referenciaDatabase = FirebaseDatabase.getInstance().getReference();
		mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapita);
		requestQueue = Volley.newRequestQueue(getActivity());
		request= Volley.newRequestQueue(getActivity());
		
		botonRutaFlotanto = view.findViewById(R.id.botonRutaFlotanto);
		
		
		
	}
	
	//*-*-*-*-*-*-* MÉTODOS NO UTILIZABLES NO ELIMINARLOS AÚN ASÍ *-*-*-*-*-*-*-*-*
	
	@Override
	public void onLocationChanged(Location location) {
		double lati = location.getLatitude();
		double longi =location.getLongitude();
		
		//enviarUbicacionFirebase(lati, longi);
		
	}
	
	private void enviarUbicacionFirebase(double lati, double longi) {
		
		Map<String, Object> latitudLongitud = new HashMap<>();
		latitudLongitud.put("Latitud", lati);
		latitudLongitud.put("Longitud", longi);
		
		referenciaDatabase.child("Usuarios").child("Alejo").setValue(latitudLongitud);
		//marcarUbicacionAmbulancia();
		
		
		
	}
	
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		
		switch (status) {
			case LocationProvider.AVAILABLE:
				Log.d("debug", "LocationProvider.AVAILABLE");
				break;
			case LocationProvider.OUT_OF_SERVICE:
				Log.d("debug", "LocationProvider.OUT_OF_SERVICE");
				break;
			case LocationProvider.TEMPORARILY_UNAVAILABLE:
				Log.d("debug", "LocationProvider.TEMPORARILY_UNAVAILABLE");
				break;
		}
		
	}
	
	@Override
	public void onProviderEnabled(String provider) {
	
	}
	
	@Override
	public void onProviderDisabled(String provider) {
		
		Toast.makeText(getActivity(), "Ubicación desactivada", Toast.LENGTH_SHORT).show();
	
	}
	
	private void marcarUbicacionMapa() {
		mapFragment.getMapAsync(new OnMapReadyCallback() {
			@Override
			public void onMapReady(final GoogleMap googleMap) {
			
				referenciaDatabase.child("Usuarios").child("Alejo").addListenerForSingleValueEvent(new ValueEventListener() {
					@Override
					public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
						String dir="";
						InformacionUbicacion informacionUbicacion = dataSnapshot.getValue(InformacionUbicacion.class);
						Location loc = new Location("");
						loc.setLatitude(informacionUbicacion.getLatitud());
						loc.setLongitude(informacionUbicacion.getLongitud());
						
						mLati=loc.getLatitude();
						mLongi =loc.getLongitude();
						
						if (loc.getLatitude() != 0.0 && loc.getLongitude() != 0.0) {
							try {
								Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
								List<Address> list = geocoder.getFromLocation(
										loc.getLatitude(), loc.getLongitude(), 1);
								if (!list.isEmpty()) {
									Address DirCalle = list.get(0);
									
									dir = DirCalle.getAddressLine(0);
								}
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
						
						MarkerOptions markerOptions  = new MarkerOptions();
						markerOptions.title(dir);
						
						markerOptions.position(new LatLng(loc.getLatitude(), loc.getLongitude()));
						//realizarSolicitudAmbulancia(dir);
						
						googleMap.addMarker(markerOptions);
						
						
						
					}
					
					@Override
					public void onCancelled(@NonNull DatabaseError databaseError) {
					
					}
				});
				
				
			}
		});
		
		
		
		/*
		
		try {
			mapFragment.getMapAsync(new OnMapReadyCallback() {
				@Override
				public void onMapReady(final GoogleMap mMap) {
					mapilla=mMap;
					consultaUbicacionFirebase(mMap);
					
					referenciaDatabase.child("Ambulancias").child("A-01").addValueEventListener(new ValueEventListener() {
						@Override
						public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
							mMap.clear(); //borro marcadores viejos
							
							InformacionUbicacion informacionUbicacion = dataSnapshot.getValue(InformacionUbicacion.class);
							Location loc = new Location("");
							loc.setLatitude(informacionUbicacion.getLatitud());
							loc.setLongitude(informacionUbicacion.getLongitud());
							
							MarkerOptions markerOptions  = new MarkerOptions();
							markerOptions.icon(bitmapDescriptorFromVector(getActivity(), R.drawable.marker_ambulancia));
							
							markerOptions.title("A-01");
							markerOptions.position(new LatLng(loc.getLatitude(), loc.getLongitude()));
							mMap.addMarker(markerOptions);
							
							latiAmbu=loc.getLatitude();
							longiAmbu=loc.getLongitude();
							
							mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(loc.getLatitude(), loc.getLongitude()), 15));
							
						}
						
						@Override
						public void onCancelled(@NonNull DatabaseError databaseError) {
						
						}
					});
					
					
				}
			});
		}catch (Exception e){
			Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
		}
		
		
		 */
	
	
	}
	
	private void consultaUbicacionFirebase(final GoogleMap mMap) {
		
		referenciaDatabase.child("Usuarios").child("Alejo").addListenerForSingleValueEvent(new ValueEventListener() {
			@Override
			public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
				mMap.clear();
				String dir="";
				InformacionUbicacion informacionUbicacion = dataSnapshot.getValue(InformacionUbicacion.class);
				Location loc = new Location("");
				loc.setLatitude(informacionUbicacion.getLatitud());
				loc.setLongitude(informacionUbicacion.getLongitud());
				
				mLati=loc.getLatitude();
				mLongi =loc.getLongitude();
				
				if (loc.getLatitude() != 0.0 && loc.getLongitude() != 0.0) {
					try {
						Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
						List<Address> list = geocoder.getFromLocation(
								loc.getLatitude(), loc.getLongitude(), 1);
						if (!list.isEmpty()) {
							Address DirCalle = list.get(0);
							
							dir = DirCalle.getAddressLine(0);
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				
				MarkerOptions markerOptions  = new MarkerOptions();
				markerOptions.title(dir);
				
				markerOptions.position(new LatLng(loc.getLatitude(), loc.getLongitude()));
				//realizarSolicitudAmbulancia(dir);
				
				mMap.addMarker(markerOptions);
				
				
				
			}
			
			@Override
			public void onCancelled(@NonNull DatabaseError databaseError) {
			
			}
		});
	
	}
	
	
	
	private void realizarSolicitudAmbulancia(String direccion) {
		
		String url= Constantes.URL_PRINCIPAL+"insertar_solicitud.php?direccion="+direccion+"&estatus="+"0";
		
		stringRequest= new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
			@Override
			public void onResponse(String response) {
				Toast.makeText(getContext(), "Solicitando ambulancia...", Toast.LENGTH_SHORT).show();
				
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				Toast.makeText(getContext(), "Se produjo un problema a la hora de solicitar la ambulancia. "+error.toString(), Toast.LENGTH_SHORT).show();
			}
		});
		
		requestQueue.add(stringRequest);
		
	}
	
	
}