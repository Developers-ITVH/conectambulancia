package com.bringsolutions.conectambulancia.ui.preguntame;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.bringsolutions.conectambulancia.Adaptadores.AdaptadorMensajes;
import com.bringsolutions.conectambulancia.objetos.Mensajes;
import com.bringsolutions.conectambulancia.R;

import java.util.ArrayList;
import java.util.List;

import ai.api.AIDataService;
import ai.api.AIServiceException;
import ai.api.android.AIConfiguration;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;
import ai.api.model.Result;

public class PreguntameFragment extends Fragment {
	
	private PreguntameViewModel preguntameViewModel;
	//Elementos principales
	FloatingActionButton button;
	EditText cajaMensajes;
	Mensajes mensaje;
	//Recicler de Mensajes
	LinearLayoutManager linearLayoutManager;
	private RecyclerView recyclerView;
	private AdaptadorMensajes adapter;
	List<Mensajes> MENSAJES = new ArrayList<>() ;


	//Elementos del Botz
	AIConfiguration config;
	AIDataService aiDataService;
	AIRequest aiRequest;
	View view;

	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		 view = inflater.inflate(R.layout.fragment_preguntame, container, false);

		casting();
		iniciarBot();
		return view;
	}


	public void casting ()
	{


		//casting
		recyclerView = view.findViewById(R.id.reciclerchat);
		button = view.findViewById(R.id.buttonchat);
		cajaMensajes  = view.findViewById(R.id.txtmsg);
		//Asiganando layout manager
		linearLayoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL, true);
		//listener al boton
		button.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				//berifaciondo si la caja de texto esta vacia
				if (cajaMensajes.getText().toString().isEmpty())
				{
					//Mensaje en caso de estar vacio !1
					Toast.makeText(getContext(), "Favor de escribir un mensaje por favor !! ", Toast.LENGTH_SHORT).show();
				}else
				{
					//nuevo mensaje de usuario obtendio de l acaja de texto
					mensaje = new Mensajes("Usuario",cajaMensajes.getText().toString());
					//Limpiando caja
					cajaMensajes.setText(null);
					//Mensajes del usuario al recicler
					mandarMensajeAlRecicler(mensaje);
					//Mandando mensajes al bot
					mandarMesajeBot(mensaje);
				}


			}
		});

		//Mensaje de vienvenida del bot
		mandarMensajeAlRecicler(new Mensajes("Bot","Hola Soy Preguntabot , tu asesor Virtual  puedes preguntarme lo que gustes "));

	}

	private void iniciarBot()
	{
		//configuracions del bot

		//Token
		//Lenguage
		//Metodo de reconocimiento
		config = new AIConfiguration(("a371a1ce8fa6449d9e30cee8ad57281e"),
				AIConfiguration.SupportedLanguages.Spanish,
				AIConfiguration.RecognitionEngine.System);

		//Mandano configuraciones al servicio del bot
		aiDataService = new AIDataService(config);
		//Inicializando AIrequest
		aiRequest = new AIRequest();


	}

	private void mandarMensajeAlRecicler(Mensajes mensajes)
	{

		//Establecer diseño inverso
		linearLayoutManager.setReverseLayout(false);
		//Establecer pila desde el final
		linearLayoutManager.setStackFromEnd(false);



		//Añadiendo Mensaje
		MENSAJES.add(mensajes);
		//Asignando adaptor de mensajes a la variable adapter
		adapter = new AdaptadorMensajes(MENSAJES,getContext());
		//Asingnndo el aaptador al reciclerview
		recyclerView.setAdapter(adapter) ;
		//Mandar a la ultima posicion
		linearLayoutManager.scrollToPosition(adapter.getItemCount()-1);
		//Asignando layoutmanager al rcicler
		recyclerView.setLayoutManager(linearLayoutManager);
		//posicionar el scrroll en el ultimo elemento

	}

	private void mandarMesajeBot(Mensajes mensajes)
	{
		//mandando consulta al request
		aiRequest.setQuery(mensajes.getMensaje());
		//ejecutar AsyncTask
	try{	TaskReqest();}catch (Exception e ){
		Toast.makeText(getContext(), ""+e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
	}
	}

	private void TaskReqest()
	{

		new AsyncTask<AIRequest, Void, AIResponse>() {
			@Override
			protected AIResponse doInBackground(AIRequest... requests) {
				//Asignando request
				final AIRequest request = requests[0];
				try {
					//Obteniendo respuesta del resrvicio al ejecutar el request
					final AIResponse response = aiDataService.request(aiRequest);

					//retornando la respesta
					return response;
				} catch (AIServiceException e) {
					// Por si ay algun error
					System.out.println("Error :" +e.toString());
				}

				return null;
			}
			@Override
			protected void onPostExecute(AIResponse aiResponse) {
				if (aiResponse != null) {
					// Obteniendo respuesta
					obetenerMensajeBot(aiResponse);
				}
			}
		}.execute(aiRequest);


	}

	private void obetenerMensajeBot(AIResponse response)
	{
		//Trayendo resultados
		final Result result = response.getResult();
		//Gradando respuesta speech del Bot
		final String speech = result.getFulfillment().getSpeech();
		Mensajes mensajes = new Mensajes("Bot",speech);
		if (mensajes.getMensaje().substring(0,4).equals("Link"))
		{
			mandarMensajeAlRecicler(new Mensajes("Bot","Encontre esta Información",mensajes.getMensaje().substring(5)));
		}else
			{
				//manano los mensajes del bot al recicler
				mandarMensajeAlRecicler(new Mensajes("Bot",speech));
			}

	}



}