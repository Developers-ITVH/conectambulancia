package com.bringsolutions.conectambulancia.ui.instituciones;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.support.annotation.Nullable;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;

import com.bringsolutions.conectambulancia.R;

public class InstitucionesFragment extends Fragment {
	
	private InstitucionesViewModel institucionesViewModel;
	
	public View onCreateView(@NonNull LayoutInflater inflater,
							 ViewGroup container, Bundle savedInstanceState) {
		institucionesViewModel =
				ViewModelProviders.of(this).get(InstitucionesViewModel.class);
		View root = inflater.inflate(R.layout.fragment_instituciones, container, false);
		final TextView textView = root.findViewById(R.id.text_send);
		institucionesViewModel.getText().observe(this, new Observer<String>() {
			@Override
			public void onChanged(@Nullable String s) {
				textView.setText(s);
			}
		});
		return root;
	}
}