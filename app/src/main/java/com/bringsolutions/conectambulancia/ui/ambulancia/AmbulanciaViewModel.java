package com.bringsolutions.conectambulancia.ui.ambulancia;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class AmbulanciaViewModel extends ViewModel {
	
	private MutableLiveData<String> mText;
	
	public AmbulanciaViewModel() {
		mText = new MutableLiveData<>();
		mText.setValue("Ambulancia");
	}
	
	public LiveData<String> getText() {
		return mText;
	}
	
	
}