package com.bringsolutions.conectambulancia.ui.instituciones;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class InstitucionesViewModel extends ViewModel {
	
	private MutableLiveData<String> mText;
	
	public InstitucionesViewModel() {
		mText = new MutableLiveData<>();
		mText.setValue("This is send fragment");
	}
	
	public LiveData<String> getText() {
		return mText;
	}
}