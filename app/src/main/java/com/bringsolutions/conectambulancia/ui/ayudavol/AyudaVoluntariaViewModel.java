package com.bringsolutions.conectambulancia.ui.ayudavol;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class AyudaVoluntariaViewModel extends ViewModel {
	
	private MutableLiveData<String> mText;
	
	public AyudaVoluntariaViewModel() {
		mText = new MutableLiveData<>();
		mText.setValue("This is tools fragment");
	}
	
	public LiveData<String> getText() {
		return mText;
	}
}