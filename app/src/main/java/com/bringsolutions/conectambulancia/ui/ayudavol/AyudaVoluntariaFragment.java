package com.bringsolutions.conectambulancia.ui.ayudavol;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import com.bringsolutions.conectambulancia.R;
import com.bringsolutions.conectambulancia.fragmentsdoctores.MedicosGenerales;
import com.bringsolutions.conectambulancia.fragmentsdoctores.Veterinarios;

public class AyudaVoluntariaFragment extends Fragment {
	View view;
	FrameLayout contenedor;
	LinearLayout linearBase;
	TabLayout tabs;
	
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		view = inflater.inflate(R.layout.fragment_ayuda_voluntaria, container, false);
		tabs = (TabLayout) view.findViewById(R.id.tabsVendedores);
		tabs.setTabMode(TabLayout.MODE_SCROLLABLE);
		contenedor = view.findViewById(R.id.contenedorDoctores);
		linearBase = view.findViewById(R.id.linearBase);
		
		tabs.addTab(tabs.newTab().setText("Médicos Generales"));
		tabs.addTab(tabs.newTab().setText("Veterinarios"));
		
		FragmentManager fm = getFragmentManager();
		fm.beginTransaction().replace(R.id.contenedorDoctores, new MedicosGenerales()).addToBackStack(null).commit();
		
		tabs.setOnTabSelectedListener(
				new TabLayout.OnTabSelectedListener() {
					@Override
					public void onTabSelected(TabLayout.Tab tab) {
						
						int selecionPestana = tab.getPosition();
						FragmentManager fm = getFragmentManager();
						
						switch (selecionPestana){
							case 0:
								fm.beginTransaction().replace(R.id.contenedorDoctores, new MedicosGenerales()).addToBackStack(null).commit();

								break;
							case 1:
								fm.beginTransaction().replace(R.id.contenedorDoctores, new Veterinarios()).addToBackStack(null).commit();
								
								break;
								
						}
						
					}
					
					@Override
					public void onTabUnselected(TabLayout.Tab tab) {
						// ...
					}
					
					@Override
					public void onTabReselected(TabLayout.Tab tab) {
						// ...
					}
					
					
				}
		);
		return view;
		
	}
}