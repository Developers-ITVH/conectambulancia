package com.bringsolutions.conectambulancia.ui.quehacer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import com.bringsolutions.conectambulancia.R;

public class QueHacerFragment extends Fragment {
	View view;
	
	private WebView browser;
	private ProgressBar progressBar;
	public View onCreateView(@NonNull LayoutInflater inflater,
							 ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fragment_quehacer, container, false);
		
		
		// Definimos el webView
		browser=(WebView)view.findViewById(R.id.webView);
		
		//Habilitamos JavaScript
		browser.getSettings().setJavaScriptEnabled(true);
		
		//Habilitamos los botones de Zoom
		browser.getSettings().setBuiltInZoomControls(true);
		
		browser.setWebViewClient(new WebViewClient() {
			
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}
		});
		
		// Cargamos la web
		browser.loadUrl("https://www.unam.mx/medidas-de-emergencia/que-hacer-en-caso-de");
		
		//Sincronizamos la barra de progreso de la web
		progressBar = (ProgressBar) view.findViewById(R.id.progressbar);
		
		browser.setWebChromeClient(new WebChromeClient() {
			@Override
			public void onProgressChanged(WebView view, int progress) {
				progressBar.setProgress(0);
				progressBar.setVisibility(View.VISIBLE);
				getActivity().setProgress(progress * 1000);
				
				progressBar.incrementProgressBy(progress);
				
				if (progress == 100) {
					progressBar.setVisibility(View.GONE);
				}
			}
		});
		
		
		
		
		
		return view;
	}
	
	
}

