package com.bringsolutions.conectambulancia.fragmentsdoctores;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bringsolutions.conectambulancia.R;
import com.bringsolutions.conectambulancia.objetos.AdaptadorDoctores;
import com.bringsolutions.conectambulancia.objetos.Constantes;
import com.bringsolutions.conectambulancia.objetos.Doctor;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class MedicosGenerales extends Fragment {
	View view;
	private RecyclerView recyclerView;
	private AdaptadorDoctores adapter;
	List<Doctor> DOCTORES = new ArrayList<>();
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		view = inflater.inflate(R.layout.fragment_medicos_generales, container, false);
		
		recyclerView = view.findViewById(R.id.recyclerMedicosGenerales);
		recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
		llenarRecycler("Médico General");
		
		return view;
	}
	
	private void llenarRecycler(String tipo) {
		
		//Crear un objeto requetqueue
		RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
		
		//Url del webservice
		String url = Constantes.URL_PRINCIPAL + "traer_doctores_xtipo.php?tipo=" + tipo;
		
		// crear un objeto requet
		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response) {
				
				JSONArray json = response.optJSONArray("doctores");
				JSONObject jsonObject = null;
				
				try {
					
					for (int i = 0; i < json.length(); i++) {
						jsonObject = json.getJSONObject(i);
						
						String tipo = jsonObject.optString("doc_tipo");
						String nombre = jsonObject.optString("doc_nombre");
						String horario = jsonObject.optString("doc_horario");
						String descripcion = jsonObject.optString("doc_descripcion");
						String telefono = jsonObject.optString("telefono");
						
						DOCTORES.add(new Doctor(tipo, nombre, horario, descripcion, telefono));
						
						adapter = new AdaptadorDoctores(DOCTORES, getActivity());
						recyclerView.setAdapter(adapter);
						
					}
					
					
				} catch (Exception e) {
					Toast.makeText(getActivity(), "No hay usuarios en esta categoría :(", Toast.LENGTH_SHORT).show();
					
				}
				
				
			}
		}, new Response.ErrorListener() {
			@Override
			
			
			public void onErrorResponse(VolleyError error) {
				Toast.makeText(getActivity(), "No hay usuarios en esta categoría", Toast.LENGTH_SHORT).show();
				
			}
		});
		
		//añadir el objeto requet al requetqueue
		requestQueue.add(jsonObjectRequest);
		
	}
	
	
	
	
	
}
